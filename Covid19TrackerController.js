({
	doinit : function(component, event, helper) {
        console.log("called");
        component.set('v.columns', [
            {label: 'COUNTRY', fieldName: 'country', type: 'text'},
            {label: 'NEW CONFIRMED', fieldName: 'newConfirmed', type: 'text'},
            {label: 'TOTAL CONFIRMED', fieldName: 'totalConfirmed', type: 'text'},
            {label: 'NEW RECOVERED', fieldName: 'newRecovered', type: 'text'},
            {label: 'TOTAL RECOVERED', fieldName: 'totalRecovered', type: 'text'},
            {label: 'NEW DEATHS', fieldName: 'newDeaths', type: 'text'},
            {label: 'TOTAL DEATHS', fieldName: 'totalDeaths', type: 'text'},
            
        ]);

        
        
		helper.getApiData(component);
	}
})
